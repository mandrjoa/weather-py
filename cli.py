import argparse
from api import get_weather

def main():
    """
    Main function to run the CLI application.
    """
    # Create the top-level parser.
    parser = argparse.ArgumentParser(description='Get the current weather for a given city.')
    parser.add_argument('city', help='The name of the city to get the weather for.')
    args = parser.parse_args()

    # Call the get_weather() function with the provided city name.
    weather_data = get_weather(args.city)

    # Check if weather data is available.
    if weather_data is not None:
        # Extract relevant information from the weather data.
        temperature = weather_data['main']['temp']
        humidity = weather_data['main']['humidity']
        wind_speed = weather_data['wind']['speed']
        weather_description = weather_data['weather'][0]['description']

        # Print the weather information.
        print(f"Weather for {args.city}:")
        print(f"Temperature: {temperature} K")
        print(f"Humidity: {humidity}%")
        print(f"Wind Speed: {wind_speed} m/s")
        print(f"Description: {weather_description}")
    else:
        print(f"Failed to fetch weather data for {args.city}.")

if __name__ == '__main__':
    main()
