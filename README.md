# WeatherPy
Welcome to WeatherPy! This project is designed to provide real-time weather data from cities around the world. It's a Python-based application that consists of a Command-Line Interface (CLI) and a RESTful API, both used to fetch and present data from the OpenWeatherMap API in a user-friendly way.

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites
- Python 3.6 or later
- Docker (for containerization)
- An API key from OpenWeatherMap (You can get one from https://openweathermap.org/)

### Installation
1. Clone this repository to your local machine.

    ```bash
    git clone https://gitlab.com/saeids/weather-py.git
    ```

2. Install necessary libraries.
    ```bash
    pip install requests
    pip install flask
    ```

3. Set your OpenWeatherMap API key as an environment variable named `WEATHER_API_KEY`.

    ```bash
    export WEATHER_API_KEY=d35d95e5c9e485597377a9ba5ef88e3b
    ```
**Note:** Storing API keys and other sensitive information directly in code is generally not a best practice. It's included here for simplicity.

### Files:
- `cli.py`: This file contains the logic for your Command-Line Interface (CLI).
- `api.py`: This file contains the logic to communicate with the OpenWeatherMap API.

## Running the App
To run the app, navigate to the project directory and use the Python command followed by the city name:

```bash
python cli.py <city-name>
```

## Running the Tests
Tests are performed using pytest. To run the tests, navigate to the project directory and execute the following command:

```bash
pytest
```

## API Documentation
The OpenWeatherMap API is used to fetch the weather data. The base URL for the API is `http://api.openweathermap.org/data/2.5/weather`. You can get the weather of a city by appending `?q={city name}&appid={API key}` to the base URL. 

## Features
- Fetch real-time weather data for any city
- Display key weather information: temperature, humidity, wind speed, and general conditions
- RESTful API endpoint for fetching weather data
- Dockerized application for consistent performance across platforms
- Comprehensive error handling for seamless user experience

## Built With
- Python
- Docker
- OpenWeatherMap API (for weather data)

## Contributing
This section will be updated as the project progresses.

## License
This section will be updated as the project progresses.

## Acknowledgements
This project is part of a school assignment in studying DevOps and developing Python applications.


## api.py Code description
The function is defined as get_weather(city_name), where city_name is the name of the city for which weather data is requested from the OpenWeatherMap API. 
The function starts by defining the API key (API_KEY) that is required to make requests to the OpenWeatherMap API
The function creates a dictionary called params which contains the parameters required for the API request. It includes the city name "q" and the API key "appid".

The function sends a GET request to the OpenWeatherMap API. The request is made to the base_url with the params dictionary as query parameters.The response from the API is then stored in the "response" variable. If the response status code is 200 then it means that the request was successful and the weather data is then returned as a python dictionary by calling "response.json(). The dictionary contains weather info such as temperature, wind strength, humidity and more. If the response status code is not 200, then the request will be unsuccesful and not fetch any weather data, but will instead return "None". 
It is important that the requests library is installed and that a valid API key is provided (from OpenWeatherMap API). 

## cli.py Code description
The cli.py file is a script that runs a CLI app to get the current weather for a given city. It uses the argparse module to parse CLI-arguments. 
get_weather(args.city) is used to call weather data. 
If weather data for the city is available, weather data will be extracted. (Temperature, humidity, wind speed and weather description). 
The weather information is then printed. If the city for example does not exist, it will print "Failed to fetch weather data for (city)". 

## CI/CD Pipeline description
Linting: In this stage, the code is checked against PEP8 standards using the flake8 tool.
Testing: This stage runs unit tests using pytest. The tests verify the functionality of the code. 
Building: The final stage builds a Docker image based on the Dockerfile in the repository. The image is tagged with the appropriate version and pushed to the GitLab Container Registry.
