import sys
import os
# Append the parent directory of this file to the sys.path
# This allows us to import the api module from the project root directory
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from api import get_weather

def test_get_weather_returns_none_for_invalid_city():
    """
    This test function checks the behavior of the get_weather function when provided with an invalid city name.
    It asserts that the get_weather function returns None in this case, as expected.

    No parameters are required, as the test function internally calls get_weather with a hard-coded invalid city name.

    Returns:
    This function does not return a value. Instead, it uses an assertion to signal a test failure to pytest if get_weather 
    does not behave as expected.
    """
    assert get_weather('invalidcity') is None

def test_get_weather_returns_weather_data_for_valid_city():
    """
    This test function checks the behavior of the get_weather function when provided with a valid city name.
    It asserts that the get_weather function returns weather data

    Returns:
    This function does not return a value. Instead, it uses an assertion to signal a test failure to pytest if get_weather 
    does not behave as expected.
    """
    assert isinstance(get_weather('london'), dict)

    def test_get_weather_returns_none_for_connection_error():
    """
    This test function checks the behavior of the get_weather function when a connection error occurs.
    It asserts that the get_weather function returns None in this case, as expected.


    Returns:
    This function does not return a value. Instead, it uses an assertion to signal a test failure to pytest if get_weather 
    does not behave as expected.
    """
    assert get_weather('invalidcity12345') is None
